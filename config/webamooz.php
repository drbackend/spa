<?php


return [
    'passport' => [
        'secret' => env('PASSPORT_CLIENT_SECRET'),
        'id' => env('PASSPORT_CLIENT_ID'),
    ]
];
