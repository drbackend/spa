<?php


namespace App\Traits;


use App\Http\Controllers\Auth\LoginController;
use Illuminate\Http\Request;

trait TokenTrait
{
    protected $response;

    /**
     * @return mixed
     */
    protected function getContent()
    {
        return json_decode($this->response->getContent());
    }

    /**
     * @param Request $request
     * @param null $email
     * @param null $password
     * @return
     */
    protected function getToken(Request $request, $email = null, $password = null)
    {
        $req = Request::create(
            '/oauth/token',
            'post',
            [
                'client_id' => config('webamooz.passport.id'),
                'client_secret' => config('webamooz.passport.secret'),
                'grant_type' => 'password',
                'username' => $request->email ?? $email,
                'password' => $request->password ?? $password
            ]
        );

        return $this->response = app()->handle($req);
    }
}
