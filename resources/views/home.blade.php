<!DOCTYPE html>
<html lang="en" dir="rtl">

    <head>
        <link href="/css/app.css" rel="stylesheet" />
        <title>وب اموز</title>
    </head>

    <body>

    <div id="app">
        <router-view></router-view>
    </div>

    <script src="/js/app.js"></script>

    </body>

</html>
