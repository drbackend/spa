import NotFound from "../views/errors/NotFound";
import AccessDenied from "../views/errors/AccessDenied";
import auth from "../middleware/auth";
import guest from "../middleware/guest";

const AppLayout = () => import( /* webpackChunkName: "js/app-layout" */ '../views/layout/AppLayout.vue');
const DashboardLayout = () => import( /* webpackChunkName: "js/dashboard-layout" */ '../views/layout/DashboardLayout.vue');
const AdminDashboardLayout = () => import( /* webpackChunkName: "js/admin-dashboard-layout" */ '../views/layout/AdminDashboardLayout.vue');
const Home = () => import( /* webpackChunkName: "js/home" */ '../views/Home.vue');
const AuthRoutes = () => import( /* webpackChunkName: "js/auth-routes" */ '../views/Auth/AuthRoutes.vue');
const UserDashboard = () => import( /* webpackChunkName: "js/user-dashboard" */ '../views/Dashboard/Index.vue');
const UserDashboardRoutes = () => import( /* webpackChunkName: "js/user-dashboard-routes" */ '../views/Dashboard/DashboardRoutes.vue');
const Profile = () => import( /* webpackChunkName: "js/profile-dashboard" */ '../views/Dashboard/Profile.vue');
const AdminDashboard = () => import( /* webpackChunkName: "js/admin-dashboard" */ '../views/Admin/AdminDashboard.vue');
const AdminUserRoutes = () => import( /* webpackChunkName: "js/admin-user-routes" */ '../views/Admin/User/AdminUserRoutes.vue');
const AdminUserIndex = () => import( /* webpackChunkName: "js/admin-user" */ '../views/Admin/User/Index.vue');

export default [
    {
        path: '/',
        component: AppLayout,
        children: [
            {
                path: '/',
                name: 'home',
                component: Home
            },
            {
                path: 'auth/:url',
                name: 'auth',
                component: AuthRoutes,
                props: true,
                meta: {
                    middleware: [
                        guest
                    ]
                }
            }
        ]
    },
    {
        path: '/dashboard',
        component: DashboardLayout,
        children: [
            {
                path: '',
                name: 'dashboard',
                component: UserDashboard,
            },
            {
                path: 'profile',
                name: 'profile',
                component: Profile,
            },
            {
                path: ':url',
                name: 'dashboard',
                component: UserDashboardRoutes,
                props: true
            },
        ],
        meta: {
            middleware: [
                auth
            ]
        }
    },
    {
        path: '/admin',
        component: AdminDashboardLayout,
        children: [
            {
                path: 'dashboard',
                name: 'admin-dashboard',
                component: AdminDashboard,
            },
            {
                path: 'dashboard/:url',
                name: 'admin-dashboard-routes',
                component: AdminUserRoutes,
                props: true
            },
            {
                path: 'user',
                name: 'admin-user',
                component: AdminUserIndex,
            }
        ],
        meta: {
            middleware: [
                auth
            ]
        }
    },
    {
        path: '/404',
        name: 'not-found',
        component: NotFound
    },
    {
        path: '/403',
        name: 'access-denied',
        component: AccessDenied
    },
    {
        path: '*',
        name: 'error404',
        component: NotFound
    }
]
