require('./bootstrap');
require('bootstrap-material-design');
require('./plugin/material-dashboard.min.js');

import PerfectScrollbarPlugin from "vue2-perfect-scrollbar";
require('vue2-perfect-scrollbar/src/style.css');
import router from './router/router.js'
import store from './store/store.js'

import VueMeta from 'vue-meta';

window.Vue = require('vue');

Vue.use(PerfectScrollbarPlugin);
Vue.use(VueMeta);

const files = require.context('./components', true, /Base[A-Z]\w+\.vue$/i);
files.keys().map(key => Vue.component(key.split('/').pop().split('.')[0], files(key).default));


const app = new Vue({
    el: '#app',
    router,
    store,
});
